﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Bolt;
using Ludiq;
using DG.Tweening;

public class Bag : GridItem {

    public BoxCollider2D col;
    public Dictionary<string, bool> targetObjects;
    public Dictionary<GameObject, Vector2> bagContent;
    public float animationLength;

    public GameObject ending;

    private Vector3 bagUpperLeftCoord;

    // Start is called before the first frame update
    void Start() {
        base.Init();
        bagContent = new Dictionary<GameObject, Vector2>();
        targetObjects = new Dictionary<string, bool>();
        col.size = new Vector2(base.nCols, base.nRows);
        bagUpperLeftCoord = gameObject.transform.position + new Vector3( - base.nCols / 2f, base.nRows / 2f, 0);

        try {
            var someDict = (AotDictionary) Variables.ActiveScene.Get("DictList");
            var enumerator = someDict.GetEnumerator();
            while (enumerator.MoveNext()) {
                targetObjects[(string) enumerator.Key] = false;
            }
        } catch {
            Debug.Log("[Bag.cs] In test mode");
        }
    }

    void OnTriggerStay2D(Collider2D col) {
        if (col.tag == "Item") {
            GridItem itemGrid = col.transform.gameObject.GetComponent<GridItem>();
            if (itemGrid.isPicked == false) {
                int targetX, targetY; // the grid coord in bag for the upper left grid of the item

                Vector3 itemUpperLeftCoord = col.transform.position;
                itemUpperLeftCoord += new Vector3( - itemGrid.nCols / 2f, itemGrid.nRows / 2f, 0);

                Vector3 difference = itemUpperLeftCoord - bagUpperLeftCoord;
                targetX = (int) System.Math.Floor(difference.x);
                targetX = targetX < 0 ? 0 : targetX;
                targetX = targetX > nCols - 1 ? nCols - 1 : targetX;

                targetY = (int) System.Math.Floor( - difference.y);
                targetY = targetY < 0 ? 0 : targetY;
                targetY = targetY > nRows - 1 ? nRows - 1 : targetY;

                if (bagContent.ContainsKey(col.gameObject) == false) {

                    // Change Color if not able to fit
                    SpriteRenderer[] sprs = itemGrid.gameObject.GetComponentsInChildren<SpriteRenderer>();
                    if (base.gridObj.FitIn(itemGrid.gridObj, targetY, targetX) == false) { // if not fit
                        // Do animation: move the object out
                        DG.Tweening.Sequence seq = DOTween.Sequence();
                        Vector2 returnTo = RegisterItem.GetRandomCoordInSafeZone();
                        seq.Append(col.transform.DOMove(new Vector3(returnTo.x, returnTo.y,
                            col.transform.position.z), animationLength));
                        StartCoroutine(CloseCollider(col, animationLength));
                        return;
                    } else { // if fit
                        try {
                            string str = "Object_" + (string) Variables.Object(itemGrid.gameObject).Get("ObjectTag");
                            targetObjects[str] = true;
                            IsEnd();   
                        } catch {

                        }
                    }
                    base.UpdateOccupationStatus();
                    base.occupationStatus = base.gridObj.ToString();

                    float x_coord, y_coord;
                    x_coord =   itemGrid.nCols / 2f + targetX;
                    y_coord = - itemGrid.nRows / 2f - targetY;
                    itemGrid.gameObject.transform.position = bagUpperLeftCoord + new Vector3(x_coord, y_coord, -1);
                    bagContent[col.gameObject] = new Vector2(targetX, targetY);

                } else {
                    Vector2 oldPosition = bagContent[col.gameObject];
                    float x_coord, y_coord;
                    x_coord =   itemGrid.nCols / 2f + oldPosition.x;
                    y_coord = - itemGrid.nRows / 2f - oldPosition.y;
                    itemGrid.gameObject.transform.position = bagUpperLeftCoord + new Vector3(x_coord, y_coord, -1);
                }
            }
        }
    }

    void OnTriggerExit2D(Collider2D col) {
        if (col.tag == "Item") {
            GameObject go = col.gameObject;
            if (bagContent.ContainsKey(go)) {
                // render sprite
                SpriteRenderer[] sprs = go.GetComponentsInChildren<SpriteRenderer>();
                foreach (var spr in sprs) {
                    if (spr.tag == "Item"){
                        GridItem gridItem = go.GetComponent<GridItem>();
                        Vector2 position = bagContent[go];
                        base.gridObj.TakeOut(gridItem.gridObj, (int) position.y, (int) position.x);
                        base.UpdateOccupationStatus();
                        base.occupationStatus = base.gridObj.ToString();

                        try {
                            string str = "Object_" + (string) Variables.Object(gridItem.gameObject).Get("ObjectTag");
                            if (targetObjects.ContainsKey(str)) {
                                targetObjects[str] = false;    
                            }
                        } catch {

                        }
                    }
                }
                bagContent.Remove(go);
            }
        }
    }

    public bool IsEnd() {
        foreach (string name in targetObjects.Keys) {
            if (targetObjects[name] == false) {
                return false;
            }
        }

        Debug.Log("[Bag.cs] All item packed!");

        GameEnds();
        return true;
    }

    public void GameEnds() {
        ending.GetComponent<StartVideo>().PlayVideo();
    }

    IEnumerator CloseCollider(Collider2D col, float time) {
        col.enabled = false;
        yield return new WaitForSeconds(time);
        col.enabled = true;
    }
}
