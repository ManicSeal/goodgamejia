﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DetectEscape : MonoBehaviour
{
    MouseMove _mousemove;
    bool _isEscape;
    // Start is called before the first frame update
    void Start()
    {
        _mousemove = GameObject.FindWithTag("sucker").GetComponent<MouseMove>();
        _isEscape = _mousemove.isEscape;
    }

    public void Resume()
    {
        _isEscape = false;
        _mousemove.isEscape = _isEscape;
    }
}
