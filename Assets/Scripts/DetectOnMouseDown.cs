﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectOnMouseDown : MonoBehaviour {
    void OnMouseDown() {
        if (StageManager.placing == true) {
            GridItem gi = transform.gameObject.GetComponent(typeof(GridItem)) as GridItem;
            gi.isPicked = true;   
        }
    }
}
