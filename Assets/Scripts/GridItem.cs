﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridItem : MonoBehaviour {

    public bool isPicked;

    public GameObject prefab;

    public int nRows;
    public int nCols;

    public GridObject gridObj;

    public int sortingOrderOfOccTex = 1;

    [Multiline(5)]
    public string occupationStatus;

    [SerializeField]
    public SpriteRenderer spriRend;

    private Dictionary<Vector2Int, GameObject> occTextures;

    // Start is called before the first frame update
    void Start() {
        Init();
    }

    public void Init() {
        occTextures = new Dictionary<Vector2Int, GameObject>();
        gridObj = new GridObject(nRows, nCols);
        gridObj.UpdateWithString(occupationStatus);

        spriRend.drawMode = SpriteDrawMode.Tiled;
        spriRend.size = new Vector2(nCols, nRows);

        UpdateOccupationStatus();
        // StageManager.placing = true;
    }

    void Update() {
        if (Input.GetMouseButtonUp(0)) {
            isPicked = false;
        }
        if (isPicked == true) {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.position = mousePos;
        }
    }

    public void UpdateOccupationStatus() {
        float x_coord, y_coord;
        for (int row = 0; row < nRows; row ++) {
            for (int col = 0; col < nCols; col ++) {
                Vector2Int loc = new Vector2Int(row, col);
                if (gridObj.GetOccupation(row, col)) { // if the grid is occupied
                    if (occTextures.ContainsKey(loc) == false) { // and there's no texture
                        GameObject instance = Instantiate(prefab);
                        instance.transform.parent = gameObject.transform;
                        x_coord = (-(nCols - 1) / 2f) + col;
                        y_coord = ( (nRows - 1) / 2f) - row;
                        instance.transform.localPosition = new Vector3(x_coord, y_coord, 0);
                        SpriteRenderer instanceSprRen = instance.GetComponent<SpriteRenderer>();
                        instanceSprRen.sortingOrder = sortingOrderOfOccTex;
                        occTextures[loc] = instance; // add into dictionary
                    }
                } else { // if the grid is not occupied
                    if (occTextures.ContainsKey(loc) == true) {
                        Destroy(occTextures[loc]); // remove item
                        occTextures.Remove(loc);   // remove from dictionary
                    }
                }
            }
        }
    }
}
