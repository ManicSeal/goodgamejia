﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridManager : MonoBehaviour {

    public Camera cam;

    public GameObject[] objs;

    // Start is called before the first frame update
    void Start() {
        StartCoroutine(LoadGridRelatedObject());
    } 

    public void ShowGrid() {
        foreach (GameObject obj in objs) {
            obj.SetActive(true);
        }
    }

    public void HideGrid() {
        foreach (GameObject obj in objs) {
            obj.SetActive(false);
        }
    }

    IEnumerator LoadGridRelatedObject() {
        yield return new WaitForSeconds(0.1f);
        objs = GameObject.FindGameObjectsWithTag("GridItem");
        foreach (GameObject obj in objs) {
            obj.SetActive(false);
        }
    }
}
