﻿using System;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEditor;

public class GridObject
{
    /* Parameters: 
     *     + nRows: number of rows.
     *     + nCols: number of columns.
     *     - grids: a 1d array, storing all the status;
     *              true stands for occupied.
     * ----------------------------------------------------------------------------
     * Functions:
     *     int GetPosition(Vector2Int): 
     *         translate a 2d coord into a 1d index;
     *         the 2d coord begins with row # and followed by column #;
     *         Throw IndexOutOfRangeException when given a invalid input.
     *     void SetOccupation(int x, int y, bool val):
     *         Set a coord in to a given value.
     *     bool GetOccupation(int x, int y):
     *         Get the occupation status on a given coord.
     */



    public int nRows;
    public int nCols;

    // matching a string that contains only 'X', 'O' and '\n';
    public static readonly Regex reg = new Regex(@"^[XO\n]+");

    private bool[] grids;

    public GridObject()
    {
        this.nRows = 1;
        this.nCols = 1;
        this.grids = new bool[nRows * nCols];
    }

    public GridObject(int numberOfRows, int numberOfColumns)
    {
        this.nRows = numberOfRows < 0 ? 1 : numberOfRows;
        this.nCols = numberOfColumns < 0 ? 1 : numberOfColumns;
        this.grids = new bool[nRows * nCols];
    }

    public int GetPosition(int x, int y)
    {
        // Check if the input is valid
        if (x < 0)
        {
            throw new IndexOutOfRangeException("Row index out of range, it should be greater than 0");
        }
        if (x > nRows)
        {
            throw new IndexOutOfRangeException("Row index out of range, it should be less than " + nRows.ToString());
        }

        if (y < 0)
        {
            throw new IndexOutOfRangeException("Row index out of range, it should be greater than 0");
        }
        if (y > nCols)
        {
            throw new IndexOutOfRangeException("Column index out of range, it should be less than " + nCols.ToString());
        }

        // calc the 1d index in grid
        return x * nCols + y;
    }

    public void SetOccupation(int x, int y, bool val)
    {
        grids[GetPosition(x, y)] = val;
    }

    public bool GetOccupation(int x, int y)
    {
        return grids[GetPosition(x, y)];
    }

    public bool IsFit(GridObject template, int targetX, int targetY)
    {
        if (template.nRows + targetX > nRows || template.nCols + targetY > nCols)
        {
            // if template with a offset larger than the target grid, then not fit
            return false;
        }
        for (int row = 0; row < template.nRows; row++)
        {
            for (int col = 0; col < template.nCols; col++)
            {
                if (template.GetOccupation(row, col) & this.GetOccupation(row + targetX, col + targetY))
                {
                    // if there is someting in the template and the grid has been occupied on target pos
                    // then will not fit
                    return false;
                }
            }
        }
        return true;
    }

    public bool FitIn(GridObject template, int targetX, int targetY)
    {
        if (this.IsFit(template, targetX, targetY) == false)
        {
            return false;
        }

        bool targetVal;
        for (int row = 0; row < template.nRows; row++)
        {
            for (int col = 0; col < template.nCols; col++)
            {
                targetVal = template.GetOccupation(row, col) | this.GetOccupation(row + targetX, col + targetY);
                this.SetOccupation(row + targetX, col + targetY, targetVal);
            }
        }
        return true;
    }

    public bool TakeOut(GridObject template, int targetX, int targetY) {
        // check if the template can be taken away from the position
        for (int row = 0; row < template.nRows; row++) {
            for (int col = 0; col < template.nCols; col++) {
                if (template.GetOccupation(row, col)) {
                    if (this.GetOccupation(row + targetX, col + targetY) == false) {
                        return false; // this is not the right place to take the template away.
                    }
                }
            }
        }

        // actually take away the template
        for (int row = 0; row < template.nRows; row++) {
            for (int col = 0; col < template.nCols; col++) {
                if (template.GetOccupation(row, col)) {
                    if (this.GetOccupation(row + targetX, col + targetY)) {
                        this.SetOccupation(row + targetX, col + targetY, false);
                    }
                }
            }
        }
        return true;
    }

    public int UpdateWithString(string str) {
        if (reg.IsMatch(str) == false) {
            return 1; // the string contains more than 'O', 'X' and '\n'
        }
        string denceStr = str.Replace("\n", "");
        if (denceStr.Length != nRows * nCols) {
            return 2; // the length of the string does not match the size of the grid
        }
        for (int index = 0; index < denceStr.Length; index++) {
            grids[index] = denceStr[index] == 'O' ? false : true;
        }
        return 0;
    }

    public override string ToString() {
        string str = "";
        for (int row = 0; row < nRows; row++) {
            for (int col = 0; col < nCols; col++) {
                str += this.GetOccupation(row, col) ? "X" : "O";
            }
            str += "\n";
        }
        return str;
    }
}
