﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;
public class LastVideo : MonoBehaviour
{
	public GameObject gameobj;
    string video_Path = "";//视频路径
    public VideoPlayer videoplayer;//从场景中拖入挂载VideoPlayer组件的
    private void Awake()
    {
        video_Path = Application.streamingAssetsPath  + "/last.mp4";
        videoplayer.url = video_Path;
    }
 
    public void StartPlay()
    {
        //开始或者继续播放
        videoplayer.Play();
    }
 
    public void PausePlay()
    {
        //暂停播放
        videoplayer.Pause();
    }
 
    public void StopPlay()
    {
        //停止播放
        videoplayer.Stop();
    }
 	
 	void Update()
 	{
 		if (gameobj.activeSelf)
 		{
 			StartPlay();
 		}
 	}
}
