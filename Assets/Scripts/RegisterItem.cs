﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Bolt;
using Ludiq;
public class RegisterItem : MonoBehaviour {
    public ItemManager im;
    public Camera cam;

    public float scalingFactor = 3f;
    public float animationLength = 3f;
    public float displayInterval = 1f;

    public AudioSource dropItemSound;

    public GameObject timeController;

    public Transform upperLeft;
    public Transform lowerRight;
    public static float maxX, minX, maxY, minY;

    void Start()
    {
        minX = upperLeft.position.x;
        maxY = upperLeft.position.y;
        maxX = lowerRight.position.x;
        minY = lowerRight.position.y;

        if (minX > maxX) {
            Debug.Log("[MouseMove.cs] min > max X");
        }
        if (minY > maxY) {
            Debug.Log("[MouseMove.cs] min > max Y");
        }
    }

    void OnTriggerEnter2D(Collider2D col) {
        if (col.tag == "Item") {

            im.isInSafeZone[col.gameObject] = true;

            SpriteRenderer sr = col.gameObject.GetComponent<SpriteRenderer>();
            int originPriority = sr.sortingOrder;
            sr.sortingOrder = 10;

            // do some animation
            Vector3 position = cam.transform.position;
            Transform trans = col.gameObject.transform;
            DG.Tweening.Sequence seq = DOTween.Sequence();
            seq.Append(trans.DOMove(
                           new Vector3(position.x, position.y, col.transform.position.z),
                           animationLength / 2f));
            seq.Join(trans.DOScale(Vector3.one * scalingFactor, animationLength / 2f));
            seq.AppendInterval(displayInterval);
            // TODO: move to a random place in the safe zone
            float randomPosx, randomPosy;
            randomPosx = Random.Range(minX, maxX);
            randomPosy = Random.Range(minY, maxY);
            seq.Append(trans.DOMove(new Vector3(randomPosx, randomPosy, trans.position.z), animationLength / 2f));
            seq.Join(trans.DOScale(trans.localScale, animationLength / 2f));

            StartCoroutine(TimePause(sr, originPriority));
            //play audio
            dropItemSound.Play(0);
            // Debug.Log("[RegisterItem.cs] item inside!!!!");
        }
    }

    IEnumerator TimePause(SpriteRenderer sr, int originPriority) {
        Variables.Object(timeController).Set("isPause", true);
        yield return new WaitForSeconds(animationLength + displayInterval);
        Variables.Object(timeController).Set("isPause", false);
        sr.sortingOrder = originPriority;
    }

    public static Vector2 GetRandomCoordInSafeZone() {
        return new Vector2(Random.Range(minX, maxX), Random.Range(minY, maxY));
    }
}
