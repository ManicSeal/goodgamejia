﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class StageManager : MonoBehaviour
{
    public static bool placing = false;
    public GameObject sucker;
    public GameObject suckerAudio;
    public GameObject canvas;
    public GameObject timers;
    public GameObject safeZoneCollider;
    public GameObject itemGroup;
    public GameObject airWalls;
    public GameObject oneKeyLeave;
    public CinemachineVirtualCamera packing_cam;
    public GridManager gm;

    public void ToPlacingStage() {
        packing_cam.Priority = 12; // any number greater than 10 will work;
        placing = true;
        sucker.SetActive(false);
        suckerAudio.SetActive(false);
        canvas.SetActive(false);
        timers.SetActive(false);
        safeZoneCollider.SetActive(false);
        airWalls.SetActive(true);
        foreach (Collider2D col in itemGroup.GetComponentsInChildren<Collider2D>()) {
            col.enabled = true;
        }
        gm.ShowGrid();
        if (oneKeyLeave != null)
            oneKeyLeave.SetActive(true);
    }
}
