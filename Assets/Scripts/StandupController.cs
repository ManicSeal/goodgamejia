﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StandupController : MonoBehaviour
{
    public Image BedImage;
    public Image ListImage;
    Color tempcolor;
    Color tempcolor2;

    public GameObject Sucker;
    PolygonCollider2D suckerpoly;
    BoxCollider2D suckerbox;

    // Start is called before the first frame update
    void Start()
    {
        tempcolor = BedImage.color;
        tempcolor2 = ListImage.color;
        suckerpoly = Sucker.GetComponent<PolygonCollider2D>();
        suckerbox = Sucker.GetComponent<BoxCollider2D>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            Debug.Log("space key is pressed");

            //Change the transparency of the bed
            tempcolor.a = 1f;
            BedImage.color = tempcolor;

            //change the transparency of the list
            tempcolor2.a = 0.5f;
            ListImage.color = tempcolor2;

            //disable sucker
            Sucker.GetComponent<PolygonCollider2D>().enabled = false;
            Sucker.GetComponent<BoxCollider2D>().enabled = false;
        }

        if (Input.GetKeyUp("space"))
        {
            Debug.Log("space key is release");
            tempcolor.a = 0.5f;
            BedImage.color = tempcolor;

            tempcolor2.a = 0.5f;
            ListImage.color = tempcolor2;

            Sucker.GetComponent<PolygonCollider2D>().enabled = true;
            Sucker.GetComponent<BoxCollider2D>().enabled = true;
        }
    }

    public void Standup()
    {
        tempcolor.a = 1f;
        BedImage.color = tempcolor;
    }
}
