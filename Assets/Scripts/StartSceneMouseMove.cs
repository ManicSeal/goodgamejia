﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartSceneMouseMove : MonoBehaviour
{
    int depth = 5;

    public Transform movingLimitUpperLeft;
    public Transform movingLimitLowerRight;

    float maxX, minX, maxY, minY;
    float newX, newY;

    // Start is called before the first frame update
    void Start()
    {
        minX = movingLimitUpperLeft.position.x;
        maxY = movingLimitUpperLeft.position.y;
        maxX = movingLimitLowerRight.position.x;
        minY = movingLimitLowerRight.position.y;

        if (minX > maxX) {
            Debug.Log("[MouseMove.cs] min > max X");
        }
        if (minY > maxY) {
            Debug.Log("[MouseMove.cs] min > max Y");
        }
    }

    // Update is called once per frame
    void Update() { 
        Vector3 mousePos = Input.mousePosition;
        Vector3 suckerPos = Camera.main.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, depth));

        newX = suckerPos.x;
        newY = suckerPos.y;

        newX = newX < minX ? minX : newX;
        newX = newX > maxX ? maxX : newX;

        newY = newY < minY ? minY : newY;
        newY = newY > maxY ? maxY : newY;

        transform.position = new Vector3(newX, newY, suckerPos.z);
   
       
    }

}
