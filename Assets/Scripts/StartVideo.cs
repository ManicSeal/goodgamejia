﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class StartVideo : MonoBehaviour
{
    public GameObject videoplayer;
    public GameObject screen;
    public GameObject bgm;
    
    public string nameOfNextScene;

    public VideoPlayer vp;

    // Start is called before the first frame update
    void Start()
    {
        videoplayer.SetActive(false);
        screen.SetActive(false);

        vp.loopPointReached += PlayEnds;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void PlayEnds(VideoPlayer vp) {
        Debug.Log("[StartVideo] Video finished");
        SceneManager.LoadScene(nameOfNextScene);
    }

    public void PlayVideo()
    {
        videoplayer.SetActive(true);
        screen.SetActive(true);
        bgm.SetActive(false);
    }
}
