﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Suck : MonoBehaviour {

    public float force = 10f;
    public AudioSource suckerSound;

    public bool IsTimeUp;


    // Start is called before the first frame update
    void Start() {
        
    }

    // Update is called once per frame
    void Update() {

        if(Input.GetMouseButtonDown(0) == true && IsTimeUp == false)
        {
            suckerSound.Play(0);
        }

        if (Input.GetMouseButtonUp(0) == true)
        {
            suckerSound.Stop();
        }

    }

    void OnTriggerStay2D(Collider2D col) {
        if (col.gameObject.tag == "Item" & Input.GetMouseButton(0)) {
            Rigidbody2D rig = col.gameObject.GetComponent(typeof(Rigidbody2D)) as Rigidbody2D;
            rig.AddForce((gameObject.transform.position - col.transform.position) * force);
          
        }
    }

    void OnTriggerExit2D(Collider2D col) {
        if (col.gameObject.tag == "Item") {
        }
    }
}
