using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;

public class SuckTitle : MonoBehaviour
{
    public SpriteRenderer _titleImage;
    public int sucktime;
    Color tempColor;
    bool isPressed;
    public string nextScene;

    public int sceneMiddleTime;


    public GameObject openingAudio;
    public CinemachineVirtualCamera titalCam;

    public GameObject anotherSucker;
    public GameObject canvas;
    public GameObject canvasMain;
    public GameObject timers;
    // Start is called before the first frame update
    void Start()
    {
        tempColor = _titleImage.color;
    }

    // Update is called once per frame
    void Update()
    {
        if(_titleImage.color.a <= 0)
        {
            //change scene
            titalCam.Priority = 0;
            openingAudio.SetActive(false);
            StartCoroutine(DisableThis());
        }


        if(Input.GetMouseButtonDown(0) == true)
        {
            isPressed = true;
        }

        if(Input.GetMouseButtonUp(0) == true)
        {
            isPressed = false;
        }

    }

    public void OnCollisionEnter2D(Collision2D col)
    {
        if(isPressed == true)
        {
            tempColor.a -= 1f / sucktime;
            _titleImage.color = tempColor;
        }
    }

    IEnumerator DisableThis() {
        yield return new WaitForSeconds(1.0f);

        anotherSucker.SetActive(true);
        timers.SetActive(true);
        gameObject.SetActive(false);
        canvas.SetActive(false);
        canvasMain.SetActive(true);
    }
}
