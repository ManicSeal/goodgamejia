﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TotalTimerCheckList : MonoBehaviour
{
	public Dictionary<GameObject, bool> isChecked;
	public bool allchecked;
	public bool listalltick;
    // Start is called before the first frame update
    void Start()
    {
        isChecked = new Dictionary<GameObject, bool>();
        allchecked = true; 
        listalltick = false; 
    }

    // Update is called once per frame
    void Update()
    {
        foreach (KeyValuePair<GameObject, bool> item in isChecked) {
            // something not get
            if (item.Value == false) {
                 allchecked = false;
            }
        }

        // if get all things
        listalltick = allchecked;
    }
}
