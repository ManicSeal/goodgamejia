﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemManager : MonoBehaviour
{
    public Vector2 bedZoneUpperLeft;
    public Vector2 bedZoneLowerRight;

    public Dictionary<GameObject, bool> isInSafeZone;

    // Start is called before the first frame update
    void Start()
    {
        isInSafeZone = new Dictionary<GameObject, bool>();

        foreach (GameObject item in GameObject.FindGameObjectsWithTag("Item")) {
            isInSafeZone[item] = false;
        }
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown("space")) {
            RandomItemPosition();
        }
    }

    public void RandomItemPosition() {
        float randomPosx, randomPosy;
        foreach (KeyValuePair<GameObject, bool> item in isInSafeZone) {
            if (item.Value == false) {
                randomPosx = Random.Range(bedZoneUpperLeft.x, bedZoneLowerRight.x);
                randomPosy = Random.Range(bedZoneLowerRight.y, bedZoneUpperLeft.y);
                item.Key.transform.position = new Vector2(randomPosx, randomPosy);   
            }
        }
    }
}
