# Lost under the bed

IT WAS THERE! It was just in the bed a few hours ago, but still, you lost it. How many times has that happened to you? Have you ever wondered where they go? They are under the bed. Forgotten, covered with dusts, and waiting to be found.

## Credits:
**Programming**: Junxian Liu, Hongxi Chen, Pengbo Li

**Art & Game Design**: Allie Xiao, Jiaqi Liu

**Sound & Music**: Jiaqi Liu

**Special Thanks**: Runze Zhou

## these packages are used
+ Bolt
+ DOTween
+ Lightweight RP
+ TextMesh Pro

## Download .zip for PC, Linux, Mac and Standalone
[Download | 下载](https://gitlab.com/ManicSeal/goodgamejia/-/tree/master/Release/PcMacLinux&Standalone.zip)

# 大吉
## 大吉
### 大吉
#### 大吉
##### 大吉
###### 大吉